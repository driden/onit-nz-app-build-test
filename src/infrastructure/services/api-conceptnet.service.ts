import { HttpException, Inject } from '@nestjs/common';
import { EdgeEntity } from 'src/entity/edge.entity';
import { INetworkService } from '../../interface/network.service.i';
import { IApiService } from '../../interface/api.service.i';
import * as pThrottle from 'p-throttle';
import { from, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { NodeEntity } from 'src/entity/node.entity';
import { Utils } from '../utils/common.util';

export class ConceptNetApiService implements IApiService {
  private readonly API_BASE_URL: string = 'http://api.conceptnet.io';

  private throttled: any; // throttled function

  private filteredEdges: EdgeEntity[];
  private backtrackingArray: string[];
  private arrayResult: any[];

  @Inject('SERVICE_NETWORK')
  private readonly networkService: INetworkService;

  constructor() {
    // add a throttled function
    this.throttled = pThrottle({ limit: 10, interval: 1000 })(
      async (term: string, limit: number) => await this.requestAPI(term, limit),
    );
  }

  /**
   * work queue
   * @param edges
   */
  private async queue(edges: EdgeEntity[], limit: number) {
    for (let i = 0; i < edges.length; i++) {
      const edge = edges[i];
      await this.throttled(edge.end.term, limit);
    }
  }

  /**
   * Request json-ld document from conceptNet
   * @param url
   * @returns
   */
  private async requestAPI(term: string, limit: number): Promise<any> {
    try {
      // Begin to get the first edge list
      const result = await this.networkService.get(this.getURL(term, limit));
      // return edges which the starting word is the current target word
      const observable = this.observableEdges(term, result.edges);
      const edges = await this.filterIsAConditionOnly(observable);
      // if edges length is zero, do not need to queue the API request
      if (edges.length > 0) {
        this.filteredEdges = this.filteredEdges.concat(edges);
        await this.queue(edges, limit);
      }
    } catch (e) {
      if (typeof e.name === 'number') {
        throw new HttpException(e.message, e.name);
      } else throw e;
    }
  }

  /**
   * an override method that the controller trigger the initial work
   * @param word
   * @param rootFirst
   * @param limit
   */
  public async startProcessing(
    word: string,
    rootFirst: boolean,
    limit: number,
  ) {
    // init values
    this.filteredEdges = [];
    this.backtrackingArray = [];
    this.arrayResult = [];

    const searchingTerm = `/c/en/${word}`;
    // get all edge information from the API
    await this.throttled(searchingTerm, limit);
    // array of node information (from child to parent)
    this.insertNodesSequentialOrder(this.filteredEdges, searchingTerm);
    // reverse from parent to child -> create object result
    return Utils.arraysToObjects(this.arrayResult, rootFirst);
  }

  /**
   * compare an array item in an array to the target array item
   * @param first
   * @param second
   * @returns
   */
  private isArrayToArrayEqual = (first, second) =>
    JSON.stringify(first) === JSON.stringify(second);
  /**
   * backtracking function to add array items into the array.
   * @param edges
   * @param word
   * @param newObject
   */
  private insertNodesSequentialOrder(
    edges: EdgeEntity[],
    searchingTerm: string,
  ): any {
    let hasHit = false;
    for (let i = 0; i < edges.length; i++) {
      if (edges[i].start.term === searchingTerm) {
        hasHit = true;
      }
    }
    if (!hasHit) {
      this.backtrackingArray.push(Utils.removeObjects(searchingTerm));
      const item = JSON.parse(JSON.stringify(this.backtrackingArray)).reverse();
      if (
        !this.arrayResult.some((first) => this.isArrayToArrayEqual(first, item))
      ) {
        this.arrayResult.push(item);
      }
      return;
    }

    for (let i = 0; i < edges.length; i++) {
      const edge = edges[i];
      if (edge.start.term === searchingTerm) {
        if (
          !Array.isArray(this.backtrackingArray) ||
          !this.backtrackingArray.includes(Utils.removeObjects(edge.start.term))
        ) {
          this.backtrackingArray.push(Utils.removeObjects(edge.start.term));
        }
        this.insertNodesSequentialOrder(edges, edge.end.term);
        this.backtrackingArray.splice(this.backtrackingArray.length - 1, 1);
      }
    }
  }

  /**
   * Process Edge Items from API results
   * @param term
   * @param edges
   * @returns
   */
  private filterIsAConditionOnly(
    observable: Observable<EdgeEntity>,
  ): EdgeEntity[] {
    const _edges: EdgeEntity[] = [];
    const subscription = observable.subscribe((_edge) => {
      _edges.push(_edge);
    });
    setTimeout(() => subscription.unsubscribe, 1000);
    // if (_edges.length > 0) _edges.sort((a: EdgeEntity, b: EdgeEntity) => b.weight - a.weight);  // ordering by weight
    return _edges;
  }
  /**
   * rxJs observable
   * @param searchingTerm
   * @param edges
   * @returns
   */
  private observableEdges(
    searchingTerm: string,
    edges: any,
  ): Observable<EdgeEntity> {
    return from(edges).pipe(
      filter((edge: any) => edge.weight >= 1), // weight
      filter((edge: any) => edge.rel['@id'] === '/r/IsA'), // IsA relationship
      filter(
        (edge: any) =>
          (edge.start['@id'] === searchingTerm ||
            edge.start['@id'] === `${searchingTerm}/n`) &&
          edge.start['language'] === 'en',
      ), // exactly match to the searching term, language is en
      filter(
        (edge: any) =>
          edge.end['@id'] !== searchingTerm &&
          edge.end['@id'] !== `${searchingTerm}/n` &&
          edge.end['language'] === 'en',
      ), // end node must not the searching term and language is en
      map((edge: any) => {
        const startNode = {
          label: edge.start.label,
          term: edge.start.term,
        } as NodeEntity;

        const endNode = {
          label: edge.end.label,
          term: edge.end.term,
        } as NodeEntity;

        const _edge = {
          start: startNode,
          end: endNode,
          weight: edge.weight,
        } as EdgeEntity;
        return _edge;
      }),
    );
  }

  /**
   * GET URL creator
   * @param word
   * @param limit
   * @returns
   */
  private getURL(term: string, limit: number) {
    const url = `${this.API_BASE_URL}${term}?rel=/r/IsA&limit=${limit}`;
    return url;
  }
}
