import axios from 'axios';
import { INetworkService } from '../../interface/network.service.i';

export class AxiosNetworkService implements INetworkService {
  async get(url: string): Promise<any> {
    const response = await axios.get(url);
    if (response.data.error) {
      const error = new Error(response.data.error.details);
      error.name = response.data.error.status;
      throw error;
    }
    return response.data;
  }
}
