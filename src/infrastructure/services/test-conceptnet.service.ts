import { IApiService } from '../../interface/api.service.i';
import { Utils } from '../utils/common.util';

export class TestConceptNetApiService implements IApiService {
  /**
   * an override method that the controller trigger the initial work
   * @param word
   * @param limit
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async startProcessing(
    word: string,
    rootFirst: boolean,
    limit?: number,
  ) {
    // reverse from parent to child -> create object result
    return Utils.arraysToObjects(array);
  }
}

const array = [
  ['artificial_intelligence', 'ai'],
  ['term', 'ai'],
  [
    'spatial_thing',
    'finite_spatial_thing',
    'container_independent_shape',
    'non_fluidlike_substance',
    'deformable_thing',
    'semisolid',
    'ai',
  ],
  [
    'tangible_thing',
    'container_independent_shape',
    'non_fluidlike_substance',
    'deformable_thing',
    'semisolid',
    'ai',
  ],
  [
    'tangible_thing',
    'non_fluidlike_substance',
    'deformable_thing',
    'semisolid',
    'ai',
  ],
  [
    'tangible_thing',
    'deformable_or_fluid_substance',
    'deformable_thing',
    'semisolid',
    'ai',
  ],
  [
    'spatial_thing',
    'finite_spatial_thing',
    'container_independent_shape',
    'semisolid',
    'ai',
  ],
  ['tangible_thing', 'container_independent_shape', 'semisolid', 'ai'],
  [
    'spatial_thing',
    'finite_spatial_thing',
    'container_independent_shape',
    'non_fluidlike_substance',
    'semisolid',
    'ai',
  ],
  [
    'tangible_thing',
    'container_independent_shape',
    'non_fluidlike_substance',
    'semisolid',
    'ai',
  ],
  ['tangible_thing', 'non_fluidlike_substance', 'semisolid', 'ai'],
];
