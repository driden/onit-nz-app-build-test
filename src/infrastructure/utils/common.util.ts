export class Utils {
  public static removeObjects(term: string): string {
    return term.replace(/^(\/c\/en\/)|(\/n)/g, '');
  }
  /**
   * rootFirst = !reverseOrder
   */
  public static arraysToObjects(
    twoDimensionalArrays: any[][],
    rootFirst = true,
  ) {
    const obj = {};
    for (let i = 0; i < twoDimensionalArrays.length; i++) {
      const subArray = rootFirst
        ? twoDimensionalArrays[i].reverse()
        : twoDimensionalArrays[i];
      let _tempObj = {};
      if (!obj[subArray[0]]) obj[subArray[0]] = {};
      _tempObj = obj[subArray[0]];
      for (let k = 1; k < subArray.length; k++) {
        if (!_tempObj[subArray[k]]) _tempObj[subArray[k]] = {};
        _tempObj = _tempObj[subArray[k]];
      }
    }
    return obj;
  }

  public static booleanify(value: string): boolean {
    return ['true', '1'].includes(value ? value.toLowerCase() : '');
  }
}
