import { NodeEntity } from './node.entity';

export interface EdgeEntity {
  start: NodeEntity;
  end: NodeEntity;
  weight: number;
}
