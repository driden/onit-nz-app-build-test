import { ConceptNetApiService } from '../services/api-conceptnet.service';
import { AxiosNetworkService } from '../services/network-axios.service';
import { TestConceptNetApiService } from '../services/test-conceptnet.service';
/**
 * dynamically define service modules because Injectable() decorator does not work with interfaces.
 */
export const servicesProviders = [
  {
    provide: 'SERVICE_NETWORK',
    useClass: AxiosNetworkService,
  },
  {
    provide: 'SERVICE_API',
    useClass: ConceptNetApiService,
  },
  {
    provide: 'SERVICE_API_TEST',
    useClass: TestConceptNetApiService,
  },
];
