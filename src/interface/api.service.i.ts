export interface IApiService {
  startProcessing(word: string, rootFirst: boolean, limit: number): any;
}
