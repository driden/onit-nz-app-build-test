# ONIT NZ App build Test

Author: Seung Hyun Myung

## 1. Overview

This is the app building test at Onit NZ.

## 2. Architecture

### 2.1. Design

Basically it follows a clean architecture using DI modules in the NestJS framework. From the controller to Service, the controller only refers the interface while, implementation is injected by the framework.

There are three entity interfaces (or dto) that provide a data type of certain objects in the API service and controller.

API service depends on the network driver. The network service is a factory service that provides http requests by which network driver you choose. 

ConceptNet API service is supposed to follow its domain use cases in implementation. Thus, the API service interface only requires to open one function to the controller.


![abstract software design](./docs/onit-diagram.png)

### 2.2. Main Libraries

1. **NestJs**: the set of framework libraries. Provides decorators and modules for DI, jest for testing
2. **axios**: Popular HTTP request tool.
3. **[p-throttle](https://www.npmjs.com/package/p-throttle)**: a Promise-returning async function that limit executing transactions in its work queue. First time, I tried making own throttling mechanism based on using `setInterval`, but having issues to release used callback functions. My concerns to choose this library were, It seems that it have been well maintained by contributors.
4. **RxJs**: Once the API call returns a list of edges, the filtering `IsA` conditions is bundled by Observable of RxJs and it is used where the actual filtering is required.


## 3. How to execute
### 3.1. Install
```sh
npm install --save-dev
```
### 3.2. Build
```sh
npm run build
```
### 3.3. Test (Controller, ApiService)
```sh
npm run test
```
### 3.4. Start
```sh
npm run start
```

## 4. API test

### 4.1. GET

{BASE_URL}/word-service/controllers/parent?word={word}&limit={limit}

1. `word` parameter accept a plain text (noun) with space or underscore.
    ```
    ?word=supreme court
    ?word=supreme_court
    ``` 
2. `limit` is a number and the range is 1 <= n <= 100. if out of the range, its value fixed as `100`.

- example http://localhost/word-service/controllers/parent?word=ai&limit=100

### 4.2. Postman collection

It is located in `/docs/` 
