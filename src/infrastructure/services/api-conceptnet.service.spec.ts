import { Test, TestingModule } from '@nestjs/testing';
import { servicesProviders } from '../providers/services.providers';
import { IApiService } from '../../interface/api.service.i';

describe('ConceptNet Result Test', () => {
  let apiService: IApiService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [...servicesProviders],
    }).compile();
    apiService = app.get<IApiService>('SERVICE_API_TEST');
  });

  describe('result', () => {
    it('should return an object grouped by parent terms', async () => {
      const result = await apiService.startProcessing('/c/en/ai', false, 100);
      expect(JSON.parse(JSON.stringify(result))).toEqual(
        JSON.parse(JSON.stringify(compare)),
      );
    });
  });
});

const compare = {
  artificial_intelligence: {
    ai: {},
  },
  term: {
    ai: {},
  },
  spatial_thing: {
    finite_spatial_thing: {
      container_independent_shape: {
        non_fluidlike_substance: {
          deformable_thing: {
            semisolid: {
              ai: {},
            },
          },
          semisolid: {
            ai: {},
          },
        },
        semisolid: {
          ai: {},
        },
      },
    },
  },
  tangible_thing: {
    container_independent_shape: {
      non_fluidlike_substance: {
        deformable_thing: {
          semisolid: {
            ai: {},
          },
        },
        semisolid: {
          ai: {},
        },
      },
      semisolid: {
        ai: {},
      },
    },
    non_fluidlike_substance: {
      deformable_thing: {
        semisolid: {
          ai: {},
        },
      },
      semisolid: {
        ai: {},
      },
    },
    deformable_or_fluid_substance: {
      deformable_thing: {
        semisolid: {
          ai: {},
        },
      },
    },
  },
};
