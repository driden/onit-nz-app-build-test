import { Test, TestingModule } from '@nestjs/testing';
import { servicesProviders } from '../providers/services.providers';
import { IApiService } from '../../interface/api.service.i';
import { IsAController } from './isa.controller';

describe('IsAController', () => {
  let isAController: IsAController;
  let apiService: IApiService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [IsAController],
      providers: [...servicesProviders],
    }).compile();
    apiService = app.get<IApiService>('SERVICE_API_TEST');
    isAController = app.get<IsAController>(IsAController);
  });

  describe('findAll', () => {
    it('should return a mocking object', async () => {
      jest
        .spyOn(apiService, 'startProcessing')
        .mockImplementation(() => _mock_result);
      expect(
        await isAController.testRequest({ word: 'supreme_court', limit: 100 }),
      ).toStrictEqual(_mock_result);
    });
  });
});

const _mock_result = {
  court: {
    appeals_court: {
      supreme_court: {},
    },
  },
};
