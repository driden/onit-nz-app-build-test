import { Controller, Get, HttpException, Inject, Query } from '@nestjs/common';
import { IsARequestEntity } from 'src/entity/isa_request.entity';
import { IApiService } from '../../interface/api.service.i';
import { Utils } from '../utils/common.util';


@Controller('word-service')
export class IsAController {
  constructor(
    @Inject('SERVICE_API') private readonly resourceService: IApiService,
  ) {}
  /**
   * REST API naming, controller resource is a function that does a specific work.
   * Named by a noun or a verb.
   * This semantically means word-service as a document resource -> has controller resource call 'parent'
   * @param query
   * @returns
   */
  @Get('/controllers/parent')
  public async testRequest(@Query() query: IsARequestEntity) {
    query.word = query.word
      .toLowerCase()
      .trim()
      .replace(/[ ]/g, '_')
      .replace(/^(\/c\/en\/)/g, '');

    console.log();
    const rootFirst = !Utils.booleanify(query.reverseOrder);
    // default max limit
    if (query.limit > 100 || query.limit <= 0)
      throw new HttpException('limit is invalid', 400);
    if (isNaN(query.limit)) query.limit = 100;

    return await this.resourceService.startProcessing(
      query.word,
      rootFirst,
      query.limit,
    );
  }
}
