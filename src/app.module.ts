import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { IsAModule } from './infrastructure/modules/isa.module';

@Module({
  imports: [IsAModule],
})
export class AppModule implements NestModule {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function
  configure(consumer: MiddlewareConsumer) {}
}
