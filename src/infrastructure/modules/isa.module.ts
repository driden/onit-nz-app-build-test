import { Module } from '@nestjs/common';
import { IsAController } from '../controllers/isa.controller';
import { servicesProviders } from '../providers/services.providers';

@Module({
  imports: [],
  providers: [...servicesProviders],
  controllers: [IsAController],
})
export class IsAModule {}
