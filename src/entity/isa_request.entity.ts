export interface IsARequestEntity {
  word: string;
  limit?: number;
  reverseOrder?: string;
}
